package com.app.mynotes.asynctask;

import android.os.AsyncTask;

import com.app.mynotes.interfaces.OnTaskCompleted;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

// Class to retrieve jokes
public class asyncDadJokeTask extends AsyncTask<String, String, String> {

    private OnTaskCompleted listener;

    public asyncDadJokeTask(){
    }

    public asyncDadJokeTask(OnTaskCompleted listener){
        this.listener=listener;
    }

    protected void onPreExecute() {
        super.onPreExecute();
    }

    // Perform tasks in background
    protected String doInBackground(String... params) {

        HttpURLConnection connection = null;
        BufferedReader reader = null;

        try {
            URL url = new URL(params[0]);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestProperty("Accept","application/json");
            connection.connect();


            InputStream stream = connection.getInputStream();

            reader = new BufferedReader(new InputStreamReader(stream));

            StringBuffer buffer = new StringBuffer();
            String line = "";

            while ((line = reader.readLine()) != null) {
                buffer.append(line+"\n");

            }

            return buffer.toString();


        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    // Pass data back to add note
    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);

        String jokeId = "";
        String jokeText = "";

        JSONObject jObject = null;
        try {
            if(result != null){
                jObject = new JSONObject(result);
                jokeText = jObject.getString("joke");
                jokeId = "DadJoke #" + jObject.getString("id");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (listener != null) {
            listener.onTaskCompleted(jokeId, jokeText);
        }
    }

}