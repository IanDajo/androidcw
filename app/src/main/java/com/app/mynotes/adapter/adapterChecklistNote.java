package com.app.mynotes.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

import com.app.mynotes.interfaces.AdapterButtonListener;
import com.app.mynotes.R;

import java.util.ArrayList;

// Adapter class for list-view
public class adapterChecklistNote extends ArrayAdapter<String> {

    // Set global variables
    private AdapterButtonListener buttonClickListener;
    private ArrayList<String> noteDetailsContainer;

    public adapterChecklistNote(Context context, ArrayList<String> content) {
        super(context, 0, content);
        noteDetailsContainer = content;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        String rowDetails = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.checklistnoteitems, parent, false);
        }

        // Set ui elements
        final EditText itemContent = (EditText) convertView.findViewById(R.id.tbxItemContent);
        itemContent.setTag(position);
        final CheckBox itemIsDone = (CheckBox)convertView.findViewById(R.id.cbxItemDone);
        Button itemUp = (Button)convertView.findViewById(R.id.btnItemUp);
        Button itemDown = (Button)convertView.findViewById(R.id.btnItemDown);
        Button itemDelete = (Button)convertView.findViewById(R.id.btnDeleteEntry);

        // Set click listeners
        itemIsDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveItemCheckbox(position, itemIsDone.isChecked());
            }
        });

        itemContent.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {
                String tempContentString = s.toString();
                int tag = (int) itemContent.getTag();
                if(position == tag){
                    saveItemText(position, tempContentString);
                }
            }
        });

        itemUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveUpItem(position);
            }
        });

        itemDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                moveDownItem(position);
            }
        });

        itemDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deleteItem(position);
            }
        });

        // Set fields according to supplied values
        String[] subItems = rowDetails.split("°");

        if(subItems[0] != null) {
            if(subItems[0].equals("1")){
                itemIsDone.setChecked(true);
            }
            else{
                itemIsDone.setChecked(false);
            }
        }

        if(subItems.length >= 2){
            if(subItems[1] != null) {
                itemContent.setText(subItems[1]);
            }
        }
        else{
            itemContent.setText("");
        }

        if(position == 0){
            itemUp.setBackgroundResource(R.drawable.ic_updisabled);
        }
        else{
            itemUp.setBackgroundResource(R.drawable.ic_arrowup);
        }
        if(position == (noteDetailsContainer.size() - 1)){
            itemDown.setBackgroundResource(R.drawable.ic_downdisabled);
        }
        else{
            itemDown.setBackgroundResource(R.drawable.ic_arrowdown);
        }

        // Return the completed view to render on screen
        return convertView;
    }

    // Set listeners
    public void SetButtonListener(AdapterButtonListener listener)
    {
        this.buttonClickListener = listener;
    }

    // Global refresh function
    public void refreshList(){

        notifyDataSetChanged();
        if (buttonClickListener != null)
        {
            buttonClickListener.containerItemCheck();
        }

    }

    // Save item checkbox state
    private void saveItemCheckbox(int position, Boolean isChecked){

        String rowContent = noteDetailsContainer.get(position);

        String[] subItems = rowContent.split("°");

        if(subItems[0] != null) {
            if(isChecked){
                subItems[0] = "1";
            }
            else{
                subItems[0] = "0";
            }
        }

        if(subItems.length >= 2){
            rowContent = subItems[0] + "°" + subItems[1];
        }
        else{
            rowContent = subItems[0] + "°";
        }

        noteDetailsContainer.set(position, rowContent);

    }

    // Save text-field content every time it is changed
    private void saveItemText(int position, String fieldContent){

        String cleanedContentField = fieldContent.replaceAll("[°•]", "");
        String rowContent = noteDetailsContainer.get(position);
        String[] subItems = rowContent.split("°");

        rowContent = subItems[0] + "°" + cleanedContentField;

        noteDetailsContainer.set(position, rowContent);

    }

    // Move item up
    private void moveUpItem(int position){

        int newPosition = position - 1;
        String tempContent = noteDetailsContainer.get(position);

        if(!(newPosition < 0)) {

            noteDetailsContainer.remove(position);

            noteDetailsContainer.add(newPosition, tempContent);

        }

        refreshList();
    }

    // Move item down
    private void moveDownItem(int position){

        int newPosition = position + 1;
        int maxLength = noteDetailsContainer.size() - 1;
        String tempContent = noteDetailsContainer.get(position);

        if(newPosition <= maxLength) {

            noteDetailsContainer.remove(position);

            noteDetailsContainer.add(newPosition, tempContent);

        }

        refreshList();

    }

    // Delete item from list
    private void deleteItem(int position){

        if (buttonClickListener != null)
        {
            buttonClickListener.containerRemoveItem(position);
        }

    }
}