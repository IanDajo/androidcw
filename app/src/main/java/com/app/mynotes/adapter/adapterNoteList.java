package com.app.mynotes.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;

import com.app.mynotes.R;

// Adapter class for listview
public class adapterNoteList extends CursorAdapter {
    public adapterNoteList(Context context, Cursor cursor) {
        super(context, cursor, 0);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return LayoutInflater.from(context).inflate(R.layout.lynoteslist, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView vName = (TextView) view.findViewById(R.id.name);
        TextView vExpDate = (TextView) view.findViewById(R.id.expdate);
        TextView vLModDate = (TextView) view.findViewById(R.id.lmoddate);

        // Get values from cursor
        Integer id = cursor.getInt(cursor.getColumnIndexOrThrow("_id"));
        String name = cursor.getString(cursor.getColumnIndexOrThrow("name"));
        String expDate = cursor.getString(cursor.getColumnIndexOrThrow("expdate"));
        String date = cursor.getString(cursor.getColumnIndexOrThrow("date"));
        String time = cursor.getString(cursor.getColumnIndexOrThrow("time"));

        // Fix datetime string
        String[] fixedTime = time.split("\\.");
        String lModDate = date + " " + fixedTime[0];

        if(expDate == null){
            expDate = "Not Set";
        }

        // Set respective fields
        vName.setText(name);
        vExpDate.setText(expDate);
        vLModDate.setText(lModDate);
    }
}