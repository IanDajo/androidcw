package com.app.mynotes.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.app.mynotes.R;
import com.app.mynotes.database.SQLDB;
import java.util.ArrayList;
import java.util.List;

// User account settings screen class
public class AccountSettingsActivity extends AppCompatActivity {

    // Define values
    private Toolbar toolbar;
    SQLDB db;
    int userId = -2;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_accountsettings);

        // Register screen controls
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Local Account Settings");
        toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar(toolbar);

        // Get database instance
        this.db = new SQLDB(this);

        // Set user id from previous activity
        setUserId();

    }

    // Set options menu
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.account_menu, menu);
        return true;
    }

    // Switch for options menu selection
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.btnback:
                exitSettings();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Set user id variable
    private void setUserId(){

        // Get the bundle
        Bundle bundle = getIntent().getExtras();

        // Extract the data
        if(bundle != null){

            userId = bundle.getInt("useridtocarry");

        }

    }

    // Exit settings and return to notes screen
    private void exitSettings(){

        Intent exitAccountSettings = new Intent(this, NotesHomeActivity.class);

        // Create the bundle
        Bundle bundle = new Bundle();

        // Add data to bundle
        bundle.putInt("useridtocarry", userId);

        // Add the bundle to the intent
        exitAccountSettings.putExtras(bundle);

        startActivity(exitAccountSettings);
        finish();

    }

}
