package com.app.mynotes.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mynotes.R;
import com.app.mynotes.adapter.adapterChecklistNote;
import com.app.mynotes.database.SQLDB;
import com.app.mynotes.interfaces.AdapterButtonListener;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

// Edit checkbox note class
public class EditAdvancedNoteActivity extends AppCompatActivity {

    // Define values
    private Toolbar toolbar;
    private CalendarView expCalender;
    private Switch calSwitch;
    private TextView noteName;
    private TextView noteNoDetails;
    private ListView noteDetailItems;
    private Button addNoteItem;
    private Button openCalender;
    private Button setDate;
    private View dvdrSetDate;
    adapterChecklistNote noteDetailsAdapter;
    SQLDB db;
    int userId = -2;
    int globalNoteId = -2;
    String retName = "";
    String retContent = "";
    String retExpDate = "";
    String loadedName = "";
    String loadedContent = "";
    Integer hasCalChanged = 0;

    int calYear;
    int calMonth;
    int calDay;
    boolean hasCalBeenTouched = false;

    List<String> noteDetailsContainer = new ArrayList<String>();
    String compactedDetailsContainer = "";

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editadvancednote);

        // Register screen elements
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Edit Checklist Note");
        toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar(toolbar);
        expCalender = (CalendarView)findViewById(R.id.cexpdate);
        expCalender.setVisibility(View.INVISIBLE);
        calSwitch = (Switch)findViewById(R.id.scompdate);
        noteName = (TextView)findViewById(R.id.txtnewnotename);
        noteNoDetails = (TextView)findViewById(R.id.txtInformNoDetails);
        noteDetailItems = (ListView)findViewById(R.id.noteDetailsSelection);
        addNoteItem = (Button)findViewById(R.id.btnAddNoteItem);
        openCalender = (Button)findViewById(R.id.btnOpenCalender);
        setDate = (Button)findViewById(R.id.btnSetDate);
        openCalender.setVisibility(View.INVISIBLE);
        setDate.setVisibility(View.INVISIBLE);
        dvdrSetDate = (View)findViewById(R.id.dvdrSetDate);
        dvdrSetDate.setVisibility(View.INVISIBLE);

        // Get database instance
        this.db = new SQLDB(this);

        // Calender show/hide system
        calSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if(calSwitch.isChecked()){
                    openCalender.setVisibility(View.VISIBLE);
                }
                else{
                    openCalender.setVisibility(View.INVISIBLE);
                    expCalender.setVisibility(View.INVISIBLE);
                    setDate.setVisibility(View.INVISIBLE);
                    dvdrSetDate.setVisibility(View.INVISIBLE);
                    noteDetailItems.setVisibility(View.VISIBLE);
                    noteDetailsAdapter.refreshList();
                }
                hasCalChanged = 1;
            }
        });

        // Get values from calender if date changed
        expCalender.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth){
                calYear = year;
                calMonth = month;
                calDay = dayOfMonth;
                hasCalBeenTouched = true;
                hasCalChanged = 1;
            }
        });

        addNoteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addItemToContainer();
            }
        });

        openCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                expCalender.setVisibility(View.VISIBLE);
                setDate.setVisibility(View.VISIBLE);
                dvdrSetDate.setVisibility(View.VISIBLE);
                noteDetailItems.setVisibility(View.INVISIBLE);
                noteNoDetails.setVisibility(View.INVISIBLE);
            }
        });

        setDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                expCalender.setVisibility(View.INVISIBLE);
                setDate.setVisibility(View.INVISIBLE);
                dvdrSetDate.setVisibility(View.INVISIBLE);
                noteDetailItems.setVisibility(View.VISIBLE);
                noteDetailsAdapter.refreshList();
            }
        });

        // Retrieve and set values from existing stored note
        initNote();

        // Initialize listView
        driveListView();
    }

    // Set options menu
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_menu, menu);
        return true;
    }

    // Switch for options menu clicks
    public boolean onOptionsItemSelected(MenuItem item){
        compactItemContainer();
        switch (item.getItemId()){
            case R.id.btncancel:
                cancelPopup();
                return true;
            case R.id.btnsavenote:
                savePopup();
                return true;
            case R.id.btndeletenote:
                deletePopup();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Pop up dialog for delete confirmation
    private void deletePopup(){

        AlertDialog.Builder builderDeletePopup = new AlertDialog.Builder(EditAdvancedNoteActivity.this);

        builderDeletePopup.setMessage("Are you sure that you want to delete \"" + loadedName + "\" ?").setTitle("Confirm Delete");

        // Option buttons
        builderDeletePopup.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User selected delete
                deleteNote();
            }
        });
        builderDeletePopup.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User selected cancel
            }
        });

        // Create AlertDialog
        AlertDialog deleteDialog = builderDeletePopup.create();

        deleteDialog.show();

    }

    // Pop up dialog for save confirmation
    private void savePopup(){

        AlertDialog.Builder builderSavePopup = new AlertDialog.Builder(EditAdvancedNoteActivity.this);

        builderSavePopup.setMessage("Save new changes?").setTitle("Save Note");

        // Option buttons
        builderSavePopup.setPositiveButton("Save", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User selected save
                editSave();
            }
        });
        builderSavePopup.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User selected cancel
            }
        });

        // Create AlertDialog
        AlertDialog saveDialog = builderSavePopup.create();

        saveDialog.show();

    }

    // Pop up dialog for cancel confirmation and check for content changes
    private void cancelPopup(){

        if((!loadedName.equals(noteName.getText().toString())) || (!loadedContent.equals(compactedDetailsContainer)) || (hasCalChanged == 1)) {

            AlertDialog.Builder builderCancelPopup = new AlertDialog.Builder(EditAdvancedNoteActivity.this);

            builderCancelPopup.setMessage("Discard new changes?").setTitle("Discard Changes");

            // Option buttons
            builderCancelPopup.setPositiveButton("Discard", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User selected discard
                    newCancel();
                }
            });
            builderCancelPopup.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User selected cancel
                }
            });

            // Create AlertDialog
            AlertDialog cancelDialog = builderCancelPopup.create();

            cancelDialog.show();
        }
        else{

            newCancel();

        }
    }

    // Get data from previous activity
    private void initNote(){

        // Get the bundle
        Bundle bundle = getIntent().getExtras();

        // Extract the data
        if(bundle != null){

            userId = bundle.getInt("useridtocarry");
            globalNoteId = bundle.getInt("noteidtocarry");

        }

        // Set and get content from database
        setContent();

    }

    // Get stored note
    private void setContent(){

        Cursor cursor = this.db.getNote(globalNoteId);

        // If Cursor contains results
        if (cursor != null) {
            while(cursor.moveToNext()) {
                retName = cursor.getString(cursor.getColumnIndex("name"));
                retContent = cursor.getString(cursor.getColumnIndex("content"));
                retExpDate = cursor.getString(cursor.getColumnIndex("expdate"));
            }
            cursor.close();
        }

        // Set fields
        noteName.setText(retName);
        compactedDetailsContainer = retContent;
        unpackItemContainer();
        loadedName = retName;
        loadedContent = retContent;

        // Calender load/set date logic
        if(retExpDate != null){

            calSwitch.setChecked(true);
            openCalender.setVisibility(View.VISIBLE);

            String cParts[] = retExpDate.split("-");

            int cDay = Integer.parseInt(cParts[2]);
            int cMonth = Integer.parseInt(cParts[1]);
            int cYear = Integer.parseInt(cParts[0]);
            cMonth = cMonth - 1;

            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, cYear);
            calendar.set(Calendar.MONTH, cMonth);
            calendar.set(Calendar.DAY_OF_MONTH, cDay);

            long milliTime = calendar.getTimeInMillis();

            // Set date on calender
            expCalender.setDate (milliTime, true, true);

        }

    }

    // Return to notes home without saving
    private void newCancel(){

        Intent cancelAddNote = new Intent(this, NotesHomeActivity.class);

        // Create the bundle
        Bundle bundle = new Bundle();

        // Add data to bundle
        bundle.putInt("useridtocarry", userId);

        // Add the bundle to the intent
        cancelAddNote.putExtras(bundle);

        startActivity(cancelAddNote);
        finish();
    }

    // Save the note
    private void editSave(){

        // Set values
        String noteFieldName = noteName.getText().toString();
        compactItemContainer();
        String noteFieldDetails = compactedDetailsContainer;
        String expDate = null;
        String resultMsg = "Note Saved!";
        Integer resultStatus = 0;

        if(noteFieldName.length() == 0){
            resultStatus = 1;
        }

        // Try block
        String Ex = "";
        try {

            // Proceed if ok (0), perform further checks and get data
            if (resultStatus == 0) {

                if(calSwitch.isChecked()){

                    if(hasCalBeenTouched) {

                        String convCalMonth = "";
                        String convCalDay = "";
                        calMonth = calMonth + 1;

                        if(calMonth <= 9){
                            convCalMonth = "0" + Integer.toString(calMonth);
                        }
                        else{
                            convCalMonth = Integer.toString(calMonth);
                        }

                        if(calDay <= 9){
                            convCalDay = "0" + Integer.toString(calDay);
                        }
                        else{
                            convCalDay = Integer.toString(calDay);
                        }

                        expDate = Integer.toString(calYear) + "-" + convCalMonth + "-" + convCalDay;

                    }
                    else{

                        if(retExpDate != null) {
                            expDate = retExpDate;
                        }
                        else{
                            // Get current date (compatible with SDK v24)
                            SimpleDateFormat currentDate = new SimpleDateFormat("yyyy-MM-dd");
                            String formattedCurrentDate = currentDate.format(new Date());

                            expDate = formattedCurrentDate;
                        }
                    }
                }

                // Try updating database, get result
                boolean updateOk = this.db.updateNoteDetails(noteFieldName, noteFieldDetails, expDate, userId, globalNoteId);
                if(!updateOk){
                    resultStatus = 2;
                }

            }

        }
        catch (Exception ex){

            Ex = ex.toString();
            resultStatus = 2;

        }

        // Show error if needed
        if(resultStatus == 2) {

            Toast error = Toast.makeText(getApplicationContext(),
                    Ex,
                    Toast.LENGTH_SHORT);
            error.show();

        }

        // Set result message if needed
        if(resultStatus == 1){
            resultMsg = "Please enter a name for your note!";
        }
        if(resultStatus == 2){
            resultMsg = "Oops, something went wrong!";
        }

        Toast savedStatus = Toast.makeText(getApplicationContext(),
                resultMsg,
                Toast.LENGTH_SHORT);
        savedStatus.show();

        // Note saved, go to notes home
        if(resultStatus == 0){
            Intent goToNotes = new Intent(this, NotesHomeActivity.class);

            // Create the bundle
            Bundle bundle = new Bundle();

            // Add data to bundle
            bundle.putInt("useridtocarry", userId);

            // Add the bundle to the intent
            goToNotes.putExtras(bundle);

            startActivity(goToNotes);
            finish();
        }

    }

    // Delete this note
    private void deleteNote(){

        // Delete note and get result
        boolean deleteStatus = this.db.removeNote(globalNoteId);

        // Delete ok, go to notes home
        if(deleteStatus){

            Toast noteDeleted = Toast.makeText(getApplicationContext(),
                    "Note Deleted!",
                    Toast.LENGTH_SHORT);
            noteDeleted.show();

            Intent goToNotes = new Intent(this, NotesHomeActivity.class);

            // Create the bundle
            Bundle bundle = new Bundle();

            // Add data to bundle
            bundle.putInt("useridtocarry", userId);

            // Add the bundle to the intent
            goToNotes.putExtras(bundle);

            startActivity(goToNotes);
            finish();

        }
        else{
            // Delete failed, inform user
            Toast noteNotDeleted = Toast.makeText(getApplicationContext(),
                    "Oops, could not delete note!",
                    Toast.LENGTH_SHORT);
            noteNotDeleted.show();

        }
    }

    // Add item to list
    private void addItemToContainer(){

        noteDetailsContainer.add("0°");

        noteDetailsAdapter.refreshList();

        noteDetailItems.smoothScrollToPosition(noteDetailsAdapter.getCount()-1);

    }

    // Initialize listView
    private void driveListView(){

        if(noteDetailsContainer.isEmpty()){
            noteNoDetails.setVisibility(View.VISIBLE);
        }
        else{
            noteNoDetails.setVisibility(View.INVISIBLE);
        }

        // Set values in adapter
        noteDetailsAdapter = new adapterChecklistNote(this, (ArrayList<String>)noteDetailsContainer);
        noteDetailsAdapter.SetButtonListener(onBtnClick);

        // Fill the list
        noteDetailItems.setAdapter(noteDetailsAdapter);

    }

    // Listener for calls from adapter
    AdapterButtonListener onBtnClick = new AdapterButtonListener() {

        // Check if no items message needs to be displayed
        @Override
        public void containerItemCheck() {
            // Check if container contains any details
            if(noteDetailsContainer.isEmpty()){
                noteNoDetails.setVisibility(View.VISIBLE);
            }
            else{
                noteNoDetails.setVisibility(View.INVISIBLE);
            }
        }

        // Remove item from list
        @Override
        public void containerRemoveItem(final int item){

            AlertDialog.Builder builderDeleteItemPopup = new AlertDialog.Builder(EditAdvancedNoteActivity.this);

            builderDeleteItemPopup.setMessage("Permanently delete this item?").setTitle("Delete Item");

            // Option buttons
            builderDeleteItemPopup.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User selected delete
                    noteDetailsContainer.remove(item);
                    noteDetailsAdapter.refreshList();
                }
            });
            builderDeleteItemPopup.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User selected cancel
                }
            });

            // Create AlertDialog
            AlertDialog deleteItemDialog = builderDeleteItemPopup.create();

            deleteItemDialog.show();

        }
    };

    // Save array to string
    private void compactItemContainer(){

        compactedDetailsContainer = "";

        int size = noteDetailsContainer.size();

        for(int i = 0; i < size; i++){

            String item = noteDetailsContainer.get(i);

            compactedDetailsContainer = compactedDetailsContainer.concat(item);
            compactedDetailsContainer = compactedDetailsContainer.concat("•");

        }

    }

    // Unpack note items string and save to global array list
    private void unpackItemContainer(){

        noteDetailsContainer.clear();

        if(!compactedDetailsContainer.equals("")){

            String[] tempItemHolder = compactedDetailsContainer.split("•");

            int size = tempItemHolder.length;

            for(int i = 0; i < size; i++) {

                String item = tempItemHolder[i];

                noteDetailsContainer.add(item);

            }

        }

    }

}
