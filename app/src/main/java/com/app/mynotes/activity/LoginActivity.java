package com.app.mynotes.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.content.Intent;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.app.mynotes.R;
import com.app.mynotes.database.SQLDB;

// Home screen class
public class LoginActivity extends AppCompatActivity {

    // Set values
    private Button loginButton;
    private Button registerButton;
    private EditText nameField;
    private EditText passwordField;
    private CheckBox stayLoggedCheckBox;
    SQLDB db;
    Integer userId = -1;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Register layout controls
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        loginButton = (Button) findViewById(R.id.btnlogin);
        registerButton = (Button) findViewById(R.id.btnreg);
        nameField = (EditText)findViewById(R.id.txtlogname);
        passwordField = (EditText)findViewById(R.id.txtlogpass);
        stayLoggedCheckBox = (CheckBox)findViewById(R.id.chbxStayLogged);

        // Get database instance
        this.db = new SQLDB(this);

        // Set click listener
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                logUserIn();
            }
        });

        // Set click listener
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                openRegisterActivity();
            }
        });

        // Check if user logged in
        checkIsUserLoggedIn();

        // Fill username field if returning from register
        fillUserField();

    }

    // Check if a user is still logged in
    private void checkIsUserLoggedIn(){

        // Get cursor
        Cursor checkForLoggedInUser = this.db.getAllUsersStayLoggedStatus();

        // Cursor contains elements?
        if(checkForLoggedInUser != null){

            while (checkForLoggedInUser.moveToNext()) {

                if(checkForLoggedInUser.getInt(checkForLoggedInUser.getColumnIndex("logged_pref")) == 1){

                    userId = checkForLoggedInUser.getInt(checkForLoggedInUser.getColumnIndex("id"));
                    String userName = checkForLoggedInUser.getString(checkForLoggedInUser.getColumnIndex("name"));

                    Toast welcomeBack = Toast.makeText(getApplicationContext(),
                            "Welcome back, " + userName,
                            Toast.LENGTH_SHORT);
                    welcomeBack.show();

                    Intent returnToNotes = new Intent(this, NotesHomeActivity.class);

                    // Create the bundle
                    Bundle bundle = new Bundle();

                    // Add data to bundle
                    bundle.putInt("useridtocarry", userId);

                    // Add the bundle to the intent
                    returnToNotes.putExtras(bundle);

                    startActivity(returnToNotes);
                    finish();

                }

            }

        }

    }

    // Go to register screen
    private void openRegisterActivity() {
        Intent registerIntent = new Intent(this, RegisterActivity.class);

        startActivity(registerIntent);
        finish();
    }

    // Fill username field if supplied by register activity
    private void fillUserField(){

        // Get the bundle
        Bundle bundle = getIntent().getExtras();

        // Extract the data
        if(bundle != null){
            String chosenUserName = bundle.getString("usernametocarry");

            if(chosenUserName != null){

                nameField.setText(chosenUserName);

            }

        }

    }

    // Log user in
    private void logUserIn(){

        // Set values
        String userNameField = nameField.getText().toString();
        String userPasswordField = passwordField.getText().toString();
        String resultMsg = "You have been successfully logged-in!";
        Integer resultStatus = 0;
        userId = -1;

        // Check fields
        if(userNameField.length() == 0){
            resultStatus = 1;
        }
        if(userPasswordField.length() == 0){
            resultStatus = 2;
        }

        // Try block
        String Ex = "";
        try {

            // Go ahead if ok (0), perform more checks and get user id
            if(resultStatus == 0){

                boolean userExists = this.db.userExists(userNameField);
                if (userExists != true) {
                    resultStatus = 3;
                }

            }

            if(resultStatus == 0){

                boolean passwordIsCorrect = this.db.isEnteredPasswordCorrect(userNameField, userPasswordField);
                if(!passwordIsCorrect){
                    resultStatus = 4;
                }

            }

            // Get the user id
            if(resultStatus == 0){

                userId = this.db.getUserId(userNameField);

                if(userId == -1){
                    resultStatus = 5;
                }
                if(userId == -3){
                    resultStatus = 5;
                }

            }

        }
        catch (Exception ex){

            Ex = ex.toString();
            resultStatus = 5;

        }

        // Show error if needed
        if(resultStatus == 5) {

            Toast error = Toast.makeText(getApplicationContext(),
                    Ex,
                    Toast.LENGTH_SHORT);
            error.show();

        }

        // Set result message
        if(resultStatus == 1){
            resultMsg = "Please enter your Username!";
        }
        if(resultStatus == 2){
            resultMsg = "Please enter your Password!";
        }
        if(resultStatus == 3){
            resultMsg = "That Username does not exist!";
        }
        if(resultStatus == 4){
            resultMsg = "Incorrect password!";
        }
        if(resultStatus == 5){
            resultMsg = "Oops, something went wrong!";
        }

        Toast welcome = Toast.makeText(getApplicationContext(),
                resultMsg,
                Toast.LENGTH_SHORT);
        welcome.show();

        // All checks good, take user id and go to notes home
        if(resultStatus == 0){

            Integer tempLoggedPref = 0;

            if(stayLoggedCheckBox.isChecked()){
                tempLoggedPref = 1;
            }
            else{
                tempLoggedPref = 0;
            }

            boolean updateLoggedStatus = this.db.updateUserStayLoggedPref(userId, tempLoggedPref);

            if(!updateLoggedStatus){
                Toast errorUpdateLoggedStatus = Toast.makeText(getApplicationContext(),
                        "Error updating keep me Logged-In preference!",
                        Toast.LENGTH_SHORT);
                errorUpdateLoggedStatus.show();
            }
            else {

                Intent goToNotes = new Intent(this, NotesHomeActivity.class);

                // Create the bundle
                Bundle bundle = new Bundle();

                // Add data to bundle
                bundle.putInt("useridtocarry", userId);

                // Add the bundle to the intent
                goToNotes.putExtras(bundle);

                startActivity(goToNotes);
                finish();
            }
        }

    }

}
