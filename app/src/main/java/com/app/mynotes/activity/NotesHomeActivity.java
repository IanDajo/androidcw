package com.app.mynotes.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mynotes.R;
import com.app.mynotes.adapter.adapterNoteList;
import com.app.mynotes.database.SQLDB;

import java.util.ArrayList;
import java.util.List;

// Note home screen class
public class NotesHomeActivity extends AppCompatActivity {

    // Define values
    private Toolbar toolbar;
    private TextView noNotesFound;
    private TextView nName;
    private TextView nBy;
    private TextView nMod;
    private View hName;
    private View hBy;
    private View hMod;
    private TextView indicatorName;
    private TextView indicatorBy;
    private TextView indicatorMod;
    private EditText searchBar;
    ListView noteItems;
    SQLDB db;
    int userId = -2;
    List<Integer> noteIdTracker = new ArrayList<Integer>();
    List<Integer> noteTypeTracker = new ArrayList<Integer>();
    Integer sortPref = 0;
    String emptyText = "";
    String arrowUp = "↑";
    String arrowDown = "↓";
    Cursor notesListCursor;
    adapterNoteList notesAdapter;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_noteshome);

        // Register screen controls
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Notes");
        toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar(toolbar);
        noteItems = (ListView)findViewById(R.id.notesselection);
        noNotesFound = (TextView)findViewById(R.id.txtinformnonotes);
        noNotesFound.setVisibility(View.INVISIBLE);
        nName = (TextView)findViewById(R.id.txtnameinfo);
        nBy = (TextView)findViewById(R.id.txtexpinfo);
        nMod = (TextView)findViewById(R.id.txtinfomoddate);
        hName = (View)findViewById(R.id.dvdrname);
        hBy = (View)findViewById(R.id.dvdrcby);
        hMod = (View)findViewById(R.id.dvdrlmod);
        hName.setVisibility(View.INVISIBLE);
        hBy.setVisibility(View.INVISIBLE);
        hMod.setVisibility(View.INVISIBLE);
        indicatorName = (TextView)findViewById(R.id.txtSortOrderIndicator3);
        indicatorBy = (TextView)findViewById(R.id.txtSortOrderIndicator1);
        indicatorMod = (TextView)findViewById(R.id.txtSortOrderIndicator2);
        searchBar = (EditText)findViewById(R.id.txtSearchBar);

        // Get database instance
        this.db = new SQLDB(this);

        // Set user id from previous activity
        setUserId();

        // Click listener for note selection
        noteItems.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                editNote(noteIdTracker.get(position), noteTypeTracker.get(position));

            }
        });

        // Set click listener
        nName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                String tempSearchString = searchBar.getText().toString();
                if(sortPref == 2){
                    fillNotesList(5, tempSearchString);
                }
                else {
                    fillNotesList(2, tempSearchString);
                }
            }
        });

        // Set click listener
        nBy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                String tempSearchString = searchBar.getText().toString();
                if(sortPref == 1){
                    fillNotesList(4, tempSearchString);
                }
                else {
                    fillNotesList(1, tempSearchString);
                }
            }
        });

        // Set click listener
        nMod.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                String tempSearchString = searchBar.getText().toString();
                if(sortPref == 0) {
                    fillNotesList(3, tempSearchString);
                }
                else{
                    fillNotesList(0, tempSearchString);
                }
            }
        });

        // Search bar update listener
        searchBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String tempSearchString = searchBar.getText().toString();
                fillNotesList(sortPref, tempSearchString);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void afterTextChanged(Editable s) {}
        });

        // Get cursor
        Cursor selectedSortPref = this.db.getUserById(userId);
        Integer tempSortPref = 0;

        // Cursor contains elements?
        if(selectedSortPref != null){

            while (selectedSortPref.moveToNext()) {

                tempSortPref = selectedSortPref.getInt(selectedSortPref.getColumnIndex("sort_pref"));

                if(tempSortPref != null){
                    sortPref = tempSortPref;
                }

            }

        }

        // Set list adapter for note list
        initNotesList();

        // Populate the notes list
        fillNotesList(sortPref, null);

    }

    // Set options menu
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        return true;
    }

    // Switch for options menu selection
    public boolean onOptionsItemSelected(MenuItem item){
        switch (item.getItemId()){
            case R.id.btnlogout:
                logUserOutConfirmation();
                return true;
            case R.id.btnsettings:
                accountSettings();
                return true;
            case R.id.btnnewstandardnote:
                addNewNote();
                return true;
            case R.id.btnnewadvancednote:
                addNewAdvancedNote();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Set user id variable
    private void setUserId(){

        // Get the bundle
        Bundle bundle = getIntent().getExtras();

        // Extract the data
        if(bundle != null){

            userId = bundle.getInt("useridtocarry");

        }

    }

    // Ask for user confirmation before logging out
    private void logUserOutConfirmation(){

        AlertDialog.Builder builderLogOutPopup = new AlertDialog.Builder(NotesHomeActivity.this);

        builderLogOutPopup.setMessage("Are you sure that you want to Log-Out?").setTitle("Log-Out");

        // Option buttons
        builderLogOutPopup.setPositiveButton("Log-Out", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User selected log-out
                logUserOut();
            }
        });
        builderLogOutPopup.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User selected cancel
            }
        });

        // Create AlertDialog
        AlertDialog logOutDialog = builderLogOutPopup.create();

        logOutDialog.show();

    }

    // Log user out and return to log in screen
    private void logUserOut(){

        boolean updateStayLoggedPref = this.db.updateUserStayLoggedPref(userId, 0);

        if(!updateStayLoggedPref){
            Toast logOutError = Toast.makeText(getApplicationContext(),
                    "Error Logging-Out!",
                    Toast.LENGTH_SHORT);
            logOutError.show();
        }
        else {

            Toast logOutInfo = Toast.makeText(getApplicationContext(),
                    "You are now Logged-Out!",
                    Toast.LENGTH_SHORT);
            logOutInfo.show();

            Intent logout = new Intent(this, LoginActivity.class);

            startActivity(logout);
            finish();
        }
    }

    // Account settings caller, go to settings screen
    private void accountSettings(){

        Intent goToSettings = new Intent(this, AccountSettingsActivity.class);

        // Create the bundle
        Bundle bundle = new Bundle();

        // Add data to bundle
        bundle.putInt("useridtocarry", userId);

        // Add the bundle to the intent
        goToSettings.putExtras(bundle);

        startActivity(goToSettings);
        finish();
    }

    // Add note caller, go to add note screen
    private void addNewNote(){

        Intent addNote = new Intent(this, AddNoteActivity.class);

        // Create the bundle
        Bundle bundle = new Bundle();

        // Add data to bundle
        bundle.putInt("useridtocarry", userId);

        // Add the bundle to the intent
        addNote.putExtras(bundle);

        startActivity(addNote);
        finish();
    }

    // Add checklist note caller, go to add note screen
    private void addNewAdvancedNote(){

        Intent addChecklistNote = new Intent(this, AddAdvancedNoteActivity.class);

        // Create the bundle
        Bundle bundle = new Bundle();

        // Add data to bundle
        bundle.putInt("useridtocarry", userId);

        // Add the bundle to the intent
        addChecklistNote.putExtras(bundle);

        startActivity(addChecklistNote);
        finish();
    }

    // Edit note caller, go to edit note screen
    private void editNote(Integer noteId, Integer noteType){

        if(noteType == 0 || noteType == null) {

            Intent goEditNote = new Intent(this, EditNoteActivity.class);

            // Create the bundle
            Bundle bundle = new Bundle();

            // Add data to bundle
            bundle.putInt("useridtocarry", userId);
            bundle.putInt("noteidtocarry", noteId);

            // Add the bundle to the intent
            goEditNote.putExtras(bundle);

            startActivity(goEditNote);
            finish();
        }
        if(noteType == 1) {

            Intent goEditChecklistNote = new Intent(this, EditAdvancedNoteActivity.class);

            // Create the bundle
            Bundle bundle = new Bundle();

            // Add data to bundle
            bundle.putInt("useridtocarry", userId);
            bundle.putInt("noteidtocarry", noteId);

            // Add the bundle to the intent
            goEditChecklistNote.putExtras(bundle);

            startActivity(goEditChecklistNote);
            finish();
        }
    }

    // Populate notes list
    private void fillNotesList(Integer sortBy, String searchString){

        // Visual logic
        if((sortBy == 0) || (sortBy == 3)){
            hMod.setVisibility(View.VISIBLE);
            hName.setVisibility(View.INVISIBLE);
            hBy.setVisibility(View.INVISIBLE);
            indicatorName.setText(emptyText);
            indicatorBy.setText(emptyText);
            if(sortBy == 0){
                indicatorMod.setText(arrowDown);
            }
            if(sortBy == 3){
                indicatorMod.setText(arrowUp);
            }
        }
        if((sortBy == 1) || (sortBy == 4)){
            hBy.setVisibility(View.VISIBLE);
            hName.setVisibility(View.INVISIBLE);
            hMod.setVisibility(View.INVISIBLE);
            indicatorName.setText(emptyText);
            indicatorMod.setText(emptyText);
            if(sortBy == 1){
                indicatorBy.setText(arrowUp);
            }
            if(sortBy == 4){
                indicatorBy.setText(arrowDown);
            }
        }
        if((sortBy == 2) || (sortBy == 5)){
            hName.setVisibility(View.VISIBLE);
            hBy.setVisibility(View.INVISIBLE);
            hMod.setVisibility(View.INVISIBLE);
            indicatorBy.setText(emptyText);
            indicatorMod.setText(emptyText);
            if(sortBy == 2){
                indicatorName.setText(arrowUp);
            }
            if(sortBy == 5){
                indicatorName.setText(arrowDown);
            }
        }

        // Save sort preference if it is not the same as in the database
        if(sortPref != sortBy){

            boolean sortUpdateStatus = this.db.updateUserSortPref(userId, sortBy);

            if(!sortUpdateStatus){

                Toast sortUpdateMsg = Toast.makeText(getApplicationContext(),
                        "Error, could not update new sort preference",
                        Toast.LENGTH_SHORT);
                sortUpdateMsg.show();

            }

        }

        sortPref = sortBy;

        // Get cursor
        notesListCursor = this.db.getNotesByUserId(userId, sortBy, searchString);

        // Cursor contains elements?
        if(notesListCursor != null){

            // Refresh notes list
            notesAdapter.swapCursor(notesListCursor);
            noteItems.setSelection(0);

            // Did we have any notes?
            if (notesListCursor.getCount() == 0){

                noNotesFound.setVisibility(View.VISIBLE);

                String nothingFoundMessage = "";

                if((searchString != null) && (!searchString.equals(""))){
                    nothingFoundMessage = "Could not find a matching title!";
                }
                else{
                    nothingFoundMessage = "Your notes list is empty, add some!";
                }

                noNotesFound.setText(nothingFoundMessage);

            }
            else{

                noNotesFound.setVisibility(View.INVISIBLE);

                noteIdTracker.clear();
                noteTypeTracker.clear();

                // Keep track of note ids
                boolean hasMovedToFirst = false;
                while (notesListCursor.moveToNext()) {

                    if(!hasMovedToFirst){
                        notesListCursor.moveToFirst();
                        hasMovedToFirst = true;
                    }

                    Integer tempNoteId = notesListCursor.getInt(notesListCursor.getColumnIndex("_id"));
                    Integer tempNoteType = notesListCursor.getInt(notesListCursor.getColumnIndex("notetype"));

                    noteIdTracker.add(tempNoteId);
                    noteTypeTracker.add(tempNoteType);

                }

            }

        }

    }

    // Set list adapter from cursor
    private void initNotesList(){

        // Set values in adapter
        notesAdapter = new adapterNoteList(this, notesListCursor);

        // Fill the list
        noteItems.setAdapter(notesAdapter);

    }

}
