package com.app.mynotes.activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.icu.text.SimpleDateFormat;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.app.mynotes.R;
import com.app.mynotes.adapter.adapterChecklistNote;
import com.app.mynotes.asynctask.asyncDadJokeTask;
import com.app.mynotes.database.SQLDB;
import com.app.mynotes.interfaces.AdapterButtonListener;
import com.app.mynotes.interfaces.OnTaskCompleted;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

// Add checkbox note class
public class AddAdvancedNoteActivity extends AppCompatActivity {

    // Define values
    private Toolbar toolbar;
    private CalendarView expCalender;
    private Switch calSwitch;
    private TextView noteName;
    private TextView noteNoDetails;
    private ListView noteDetailItems;
    private Button addNoteItem;
    private Button openCalender;
    private Button setDate;
    private View dvdrSetDate;
    adapterChecklistNote noteDetailsAdapter;
    SQLDB db;
    int userId = -2;

    int calYear;
    int calMonth;
    int calDay;
    boolean hasCalBeenTouched = false;
    String loadedName = "";
    String loadedContent = "";
    Integer hasCalChanged = 0;

    List<String> noteDetailsContainer = new ArrayList<String>();
    String compactedDetailsContainer = "";

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addadvancednote);

        // Register layout components
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Add Checklist Note");
        toolbar.setTitleTextColor(0xFFFFFFFF);
        setSupportActionBar(toolbar);
        expCalender = (CalendarView)findViewById(R.id.cexpdate);
        expCalender.setVisibility(View.INVISIBLE);
        calSwitch = (Switch)findViewById(R.id.scompdate);
        noteName = (TextView)findViewById(R.id.txtnewnotename);
        noteNoDetails = (TextView)findViewById(R.id.txtInformNoDetails);
        noteDetailItems = (ListView)findViewById(R.id.noteDetailsSelection);
        addNoteItem = (Button)findViewById(R.id.btnAddNoteItem);
        openCalender = (Button)findViewById(R.id.btnOpenCalender);
        setDate = (Button)findViewById(R.id.btnSetDate);
        openCalender.setVisibility(View.INVISIBLE);
        setDate.setVisibility(View.INVISIBLE);
        dvdrSetDate = (View)findViewById(R.id.dvdrSetDate);
        dvdrSetDate.setVisibility(View.INVISIBLE);

        // Get database instance
        this.db = new SQLDB(this);

        // Set click listeners
        calSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                if(calSwitch.isChecked()){
                    openCalender.setVisibility(View.VISIBLE);
                }
                else{
                    openCalender.setVisibility(View.INVISIBLE);
                    expCalender.setVisibility(View.INVISIBLE);
                    setDate.setVisibility(View.INVISIBLE);
                    dvdrSetDate.setVisibility(View.INVISIBLE);
                    noteDetailItems.setVisibility(View.VISIBLE);
                    noteDetailsAdapter.refreshList();
                }
                hasCalChanged = 1;
            }
        });

        expCalender.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth){
                calYear = year;
                calMonth = month;
                calDay = dayOfMonth;
                hasCalBeenTouched = true;
                hasCalChanged = 1;
            }
        });

        addNoteItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                addItemToContainer();
            }
        });

        openCalender.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                expCalender.setVisibility(View.VISIBLE);
                setDate.setVisibility(View.VISIBLE);
                dvdrSetDate.setVisibility(View.VISIBLE);
                noteDetailItems.setVisibility(View.INVISIBLE);
                noteNoDetails.setVisibility(View.INVISIBLE);
            }
        });

        setDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                expCalender.setVisibility(View.INVISIBLE);
                setDate.setVisibility(View.INVISIBLE);
                dvdrSetDate.setVisibility(View.INVISIBLE);
                noteDetailItems.setVisibility(View.VISIBLE);
                noteDetailsAdapter.refreshList();
            }
        });

        // Set user id for this activity
        setUserId();

        // Initialize listView
        driveListView();
    }

    // Set options menu
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.add_menu, menu);
        return true;
    }

    // Detect what button is pressed
    public boolean onOptionsItemSelected(MenuItem item){
        loadedName = noteName.getText().toString();
        compactItemContainer();
        loadedContent = compactedDetailsContainer;
        switch (item.getItemId()){
            case R.id.btncancel:
                cancelPopup();
                return true;
            case R.id.btnhappy:
                dadJokePopup();
                return true;
            case R.id.btnsavenote:
                newSave();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // Pop up dialog for cancel confirmation and check for content changes
    private void cancelPopup(){

        if((!loadedName.equals("")) || (!loadedContent.equals("")) || (hasCalChanged == 1)) {

            AlertDialog.Builder builderCancelPopup = new AlertDialog.Builder(AddAdvancedNoteActivity.this);

            builderCancelPopup.setMessage("Discard new note?").setTitle("Discard Note");

            // Option buttons
            builderCancelPopup.setPositiveButton("Discard", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User selected discard
                    newCancel();
                }
            });
            builderCancelPopup.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User selected cancel
                }
            });

            // Create AlertDialog
            AlertDialog cancelDialog = builderCancelPopup.create();

            cancelDialog.show();
        }
        else{

            newCancel();

        }
    }

    // Dad joke
    private void dadJokePopup(){

        new asyncDadJokeTask(taskComp).execute("https://icanhazdadjoke.com/");

    }

    // Dad joke
    OnTaskCompleted taskComp = new OnTaskCompleted() {
        @Override
        public void onTaskCompleted(String jokeId, String jokeText) {

            if(jokeId.equals("")){
                Toast jokeError = Toast.makeText(getApplicationContext(),
                        "Could not retrieve joke!",
                        Toast.LENGTH_SHORT);
                jokeError.show();
            }
            else{
                if(loadedName.equals("")){
                    noteName.setText(jokeId);
                }

                noteDetailsContainer.add("0°" + jokeId + " - " + jokeText);

                noteDetailsAdapter.refreshList();
            }
        }
    };

    // Set user id from bundle
    private void setUserId(){

        // Get the bundle
        Bundle bundle = getIntent().getExtras();

        // Extract the data
        if(bundle != null){

            userId = bundle.getInt("useridtocarry");

        }

    }

    // Cancel and return to notes home
    private void newCancel(){

        Intent cancelAddNote = new Intent(this, NotesHomeActivity.class);

        // Create the bundle
        Bundle bundle = new Bundle();

        // Add data to bundle
        bundle.putInt("useridtocarry", userId);

        // Add the bundle to the intent
        cancelAddNote.putExtras(bundle);

        startActivity(cancelAddNote);
        finish();
    }

    // Save the new note
    private void newSave(){

        // Define values
        String noteFieldName = noteName.getText().toString();
        compactItemContainer();
        String noteFieldDetails = compactedDetailsContainer;
        String expDate = null;
        String resultMsg = "Note Saved!";
        Integer resultStatus = 0;

        if(noteFieldName.length() == 0){
            resultStatus = 1;
        }

        // Try block
        String Ex = "";
        try {

            // Go ahead if status ok (0), perform checks and retrieve needed data
            if (resultStatus == 0) {

                if(calSwitch.isChecked()){

                    if(hasCalBeenTouched) {

                        String convCalMonth = "";
                        String convCalDay = "";
                        calMonth = calMonth + 1;

                        if(calMonth <= 9){
                            convCalMonth = "0" + Integer.toString(calMonth);
                        }
                        else{
                            convCalMonth = Integer.toString(calMonth);
                        }

                        if(calDay <= 9){
                            convCalDay = "0" + Integer.toString(calDay);
                        }
                        else{
                            convCalDay = Integer.toString(calDay);
                        }

                        expDate = Integer.toString(calYear) + "-" + convCalMonth + "-" + convCalDay;

                    }
                    else{
                        // Get current date (compatible with SDK v24)
                        SimpleDateFormat currentDate = new SimpleDateFormat("yyyy-MM-dd");
                        String formattedCurrentDate = currentDate.format(new Date());

                        expDate = formattedCurrentDate;
                    }
                }

                // Try to insert data and get status
                boolean insertOk = this.db.insertNewNote(noteFieldName, noteFieldDetails, expDate, userId, 1);
                if(!insertOk){
                    resultStatus = 2;
                }

            }

        }
        catch (Exception ex){

            Ex = ex.toString();
            resultStatus = 2;

        }

        // Show error if one occurred
        if(resultStatus == 2) {

            Toast error = Toast.makeText(getApplicationContext(),
                    Ex,
                    Toast.LENGTH_SHORT);
            error.show();

        }

        // Set status message
        if(resultStatus == 1){
            resultMsg = "Please enter a name for your note!";
        }
        if(resultStatus == 2){
            resultMsg = "Oops, something went wrong!";
        }

        Toast savedStatus = Toast.makeText(getApplicationContext(),
                resultMsg,
                Toast.LENGTH_SHORT);
        savedStatus.show();

        // Go to notes home if all ok
        if(resultStatus == 0){
            Intent goToNotes = new Intent(this, NotesHomeActivity.class);

            // Create the bundle
            Bundle bundle = new Bundle();

            // Add data to bundle
            bundle.putInt("useridtocarry", userId);

            // Add the bundle to the intent
            goToNotes.putExtras(bundle);

            startActivity(goToNotes);
            finish();
        }

    }

    // Add item to list
    private void addItemToContainer(){

        noteDetailsContainer.add("0°");

        noteDetailsAdapter.refreshList();

        noteDetailItems.smoothScrollToPosition(noteDetailsAdapter.getCount()-1);

    }

    // Initialize listView
    private void driveListView(){

        if(noteDetailsContainer.isEmpty()){
            noteNoDetails.setVisibility(View.VISIBLE);
        }
        else{
            noteNoDetails.setVisibility(View.INVISIBLE);
        }

        // Set values in adapter
        noteDetailsAdapter = new adapterChecklistNote(this, (ArrayList<String>)noteDetailsContainer);
        noteDetailsAdapter.SetButtonListener(onBtnClick);

        // Fill the list
        noteDetailItems.setAdapter(noteDetailsAdapter);

    }

    // Listener for calls from adapter
    AdapterButtonListener onBtnClick = new AdapterButtonListener() {

        // Check if no items message needs to be displayed
        @Override
        public void containerItemCheck() {
            // Check if container contains any details
            if(noteDetailsContainer.isEmpty()){
                noteNoDetails.setVisibility(View.VISIBLE);
            }
            else{
                noteNoDetails.setVisibility(View.INVISIBLE);
            }
        }

        // Remove item from list
        @Override
        public void containerRemoveItem(final int item){

            AlertDialog.Builder builderDeleteItemPopup = new AlertDialog.Builder(AddAdvancedNoteActivity.this);

            builderDeleteItemPopup.setMessage("Permanently delete this item?").setTitle("Delete Item");

            // Option buttons
            builderDeleteItemPopup.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User selected delete
                    noteDetailsContainer.remove(item);
                    noteDetailsAdapter.refreshList();
                }
            });
            builderDeleteItemPopup.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User selected cancel
                }
            });

            // Create AlertDialog
            AlertDialog deleteItemDialog = builderDeleteItemPopup.create();

            deleteItemDialog.show();

        }
    };

    // Save array to string
    private void compactItemContainer(){

        compactedDetailsContainer = "";

        int size = noteDetailsContainer.size();

        for(int i = 0; i < size; i++){

            String item = noteDetailsContainer.get(i);

            compactedDetailsContainer = compactedDetailsContainer.concat(item);
            compactedDetailsContainer = compactedDetailsContainer.concat("•");

        }

    }

}
