package com.app.mynotes.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.app.mynotes.R;
import com.app.mynotes.database.SQLDB;

// Register class
public class RegisterActivity extends AppCompatActivity {

    // Define values
    private Button registerButton;
    private Button cancelRegisterButton;
    private EditText nameField;
    private EditText passwordField;
    SQLDB db;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        // Register screen controls
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        registerButton = (Button) findViewById(R.id.btnreg);
        cancelRegisterButton = (Button) findViewById(R.id.btncreg);
        nameField = (EditText)findViewById(R.id.txtregname);
        passwordField = (EditText)findViewById(R.id.txtregpass);

        // Get database instance
        this.db = new SQLDB(this);

        // Set click listener
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                registerNewUser();
            }
        });

        // Set click listener
        cancelRegisterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v){
                cancelReg();
            }
        });

    }

    // Cancel the registration and return to log in
    public void cancelReg(){

        Intent returnToHome = new Intent(this, LoginActivity.class);

        startActivity(returnToHome);
        finish();
    }

    // Register new user
    public void registerNewUser(){

        // Set values
        String userNameField = nameField.getText().toString();
        String userPasswordField = passwordField.getText().toString();
        String resultMsg = "You have been successfully registered!";
        Integer resultStatus = 0;

        // Perform field checks
        if(userNameField.length() == 0){
            resultStatus = 1;
        }
        if(userPasswordField.length() == 0){
            resultStatus = 2;
        }

        // Try block
        String Ex = "";
        try {

            // Perform user detail checks
            if(resultStatus == 0){

                boolean userExists = this.db.userExists(userNameField);
                if (userExists == true) {
                    resultStatus = 4;
                }

            }

            // Insert new user and get status
            if(resultStatus == 0){

                boolean insert = this.db.insertNewUser(userNameField, userPasswordField);
                if(insert == false){
                    resultStatus = 3;
                }

            }

        }
        catch (Exception ex){

            Ex = ex.toString();
            resultStatus = 3;

        }

        // Show error message if needed
        if(resultStatus == 3) {

            Toast error = Toast.makeText(getApplicationContext(),
                    Ex,
                    Toast.LENGTH_SHORT);
            error.show();

        }

        // Set result message if needed
        if(resultStatus == 1){
            resultMsg = "Please enter a Username!";
        }
        if(resultStatus == 2){
            resultMsg = "Please enter a Password!";
        }
        if(resultStatus == 3){
            resultMsg = "Oops, something went wrong!";
        }
        if(resultStatus == 4){
            resultMsg = "That Username already exists!";
        }

        Toast welcome = Toast.makeText(getApplicationContext(),
                resultMsg,
                Toast.LENGTH_SHORT);
        welcome.show();

        // All went ok, go to login screen and pass new user name
        if(resultStatus == 0){
            Intent returnToLogin = new Intent(this, LoginActivity.class);

            // Create the bundle
            Bundle bundle = new Bundle();

            // Add data to bundle
            bundle.putString("usernametocarry", userNameField);

            // Add the bundle to the intent
            returnToLogin.putExtras(bundle);

            startActivity(returnToLogin);
            finish();
        }
    }

}
