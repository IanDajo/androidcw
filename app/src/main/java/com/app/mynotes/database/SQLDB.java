package com.app.mynotes.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.icu.text.SimpleDateFormat;

import java.util.Date;

// Database class
public class SQLDB extends SQLiteOpenHelper {

    // Database details
    private static final int DB_VERSION = 4;
    private static final String DB_NAME = "localdb";
    private static final String DB_NOTE_TABLE = "notetable";
    private static final String DB_USER_TABLE = "usertable";

    // Table columns for notes
    private static final String NOTE_ID = "id";
    private static final String NOTE_NAME = "name";
    private static final String NOTE_CONTENT = "content";
    private static final String NOTE_DATE = "date";
    private static final String NOTE_TIME = "time";
    private static final String NOTE_EXPDATE = "expdate";
    private static final String NOTE_OWNERID = "ownerid";
    private static final String NOTE_TYPE = "notetype";

    // Table columns for registered users
    private static final String USER_ID = "id";
    private static final String USER_NAME = "name";
    private static final String USER_PASSWORD = "password";
    private static final String USER_SORTPREF = "sort_pref";
    private static final String USER_KEEPLOGGED = "logged_pref";

    public SQLDB(Context context){
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        // Create user table
        String createUserTable_Query = "CREATE TABLE " + DB_USER_TABLE + "(" + USER_ID + " INTEGER PRIMARY KEY," +
                USER_NAME + " TEXT," +
                USER_PASSWORD + " TEXT," +
                USER_SORTPREF + " INTEGER," +
                USER_KEEPLOGGED + " INTEGER" + ")";

        db.execSQL(createUserTable_Query);

        // Create note table
        String createNoteTable_Query = "CREATE TABLE " + DB_NOTE_TABLE + "(" + NOTE_ID + " INTEGER PRIMARY KEY," +
                NOTE_NAME + " TEXT," +
                NOTE_CONTENT + " TEXT," +
                NOTE_DATE + " TEXT," +
                NOTE_TIME + " TEXT," +
                NOTE_EXPDATE + " TEXT," +
                NOTE_OWNERID + " INTEGER," +
                NOTE_TYPE + " INTEGER" + ")";

        db.execSQL(createNoteTable_Query);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        if(oldVersion >= newVersion) {
            return;
        }

        if ((newVersion >= 3) && !(oldVersion >= 3)) {
            String addColumn1_Query = "ALTER TABLE " + DB_USER_TABLE + " ADD COLUMN " + USER_SORTPREF + " INTEGER";
            db.execSQL(addColumn1_Query);
            String addColumn2_Query = "ALTER TABLE " + DB_USER_TABLE + " ADD COLUMN " + USER_KEEPLOGGED + " INTEGER";
            db.execSQL(addColumn2_Query);
        }

        if ((newVersion >= 4) && !(oldVersion >= 4)) {
            String addColumn1_Query = "ALTER TABLE " + DB_NOTE_TABLE + " ADD COLUMN " + NOTE_TYPE + " INTEGER";
            db.execSQL(addColumn1_Query);
        }

    }

    // Insert new user and return status
    public boolean insertNewUser(String userName, String password){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_NAME, userName);
        contentValues.put(USER_PASSWORD, password);
        long result = db.insert("usertable", null, contentValues);

        if(result==-1){
            return false;
        }
        else{
            return true;
        }

    }

    // Insert new note and return status
    public boolean insertNewNote(String noteName, String noteDetails, String noteExpDate, Integer userId, Integer noteType){

        // Get current date and time (compatible with SDK v24)
        SimpleDateFormat currentDate = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm:ss");
        String formattedCurrentDate = currentDate.format(new Date());
        String formattedCurrentTime = currentTime.format(new Date());

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NOTE_NAME, noteName);
        contentValues.put(NOTE_CONTENT, noteDetails);
        contentValues.put(NOTE_EXPDATE, noteExpDate);
        contentValues.put(NOTE_OWNERID, userId);
        contentValues.put(NOTE_TYPE, noteType);
        contentValues.put(NOTE_DATE, formattedCurrentDate);
        contentValues.put(NOTE_TIME, formattedCurrentTime);
        long result = db.insert("notetable", null, contentValues);

        if(result==-1){
            return false;
        }
        else{
            return true;
        }

    }

    // Update note details and return status
    public boolean updateNoteDetails(String noteName, String noteDetails, String noteExpDate, Integer userId, Integer noteId){

        // Get current date and time (compatible with SDK v24)
        SimpleDateFormat currentDate = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat currentTime = new SimpleDateFormat("HH:mm:ss");
        String formattedCurrentDate = currentDate.format(new Date());
        String formattedCurrentTime = currentTime.format(new Date());

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(NOTE_NAME, noteName);
        contentValues.put(NOTE_CONTENT, noteDetails);
        contentValues.put(NOTE_EXPDATE, noteExpDate);
        contentValues.put(NOTE_OWNERID, userId);
        contentValues.put(NOTE_DATE, formattedCurrentDate);
        contentValues.put(NOTE_TIME, formattedCurrentTime);
        long result = db.update("notetable", contentValues, "id="+noteId, null);

        if(result==-1){
            return false;
        }
        else{
            return true;
        }

    }

    // Get user id by given username
    public Cursor getUser(String userName) {
        SQLiteDatabase db = this.getReadableDatabase();

        String from[] = { "*" };
        String where = "name=?";
        String[] whereArgs = new String[]{userName};
        Cursor cursor = db.query("usertable", from, where, whereArgs, null, null, null, null);
        return cursor;
    }

    // Get user details by given user id
    public Cursor getUserById(Integer userId) {
        SQLiteDatabase db = this.getReadableDatabase();

        String from[] = { "*" };
        String where = "id=?";
        String[] whereArgs = new String[]{userId.toString()};
        Cursor cursor = db.query("usertable", from, where, whereArgs, null, null, null, null);
        return cursor;
    }

    // Get a note by passing note id
    public Cursor getNote(Integer noteId) {
        SQLiteDatabase db = this.getReadableDatabase();

        String from[] = { "*" };
        String where = "id=?";
        String[] whereArgs = new String[]{noteId.toString()};
        Cursor cursor = db.query("notetable", from, where, whereArgs, null, null, null, null);
        return cursor;
    }

    // Check if a username exists
    public boolean userExists(String userName){
        boolean exists = false;

        Cursor cursor = this.getUser(userName);

        // If cursor contains results
        if (cursor != null) {
            while(cursor.moveToNext()) {
                String dbUserName = cursor.getString(cursor.getColumnIndex("name"));
                if (dbUserName.equals(userName)) {
                    exists = true;
                }
            }
            cursor.close();
        }

        return exists;
    }

    // Check if given password matches stored value
    public boolean isEnteredPasswordCorrect(String userName, String password){
        boolean isCorrect = false;

        Cursor cursor = this.getUser(userName);

        // If cursor contains results
        if (cursor != null) {
            while(cursor.moveToNext()) {
                String dbUserName = cursor.getString(cursor.getColumnIndex("name"));
                if (dbUserName.equals(userName)) {
                    String dbUserPassword = cursor.getString(cursor.getColumnIndex("password"));
                    if(dbUserPassword.equals(password)){
                        isCorrect = true;
                    }
                }
            }
            cursor.close();
        }

        return isCorrect;
    }

    // Get user id by passing username
    public Integer getUserId(String userName){
        Integer userId = -1;

        Cursor cursor = this.getUser(userName);

        // If cursor contains results
        if (cursor != null) {
            while(cursor.moveToNext()) {
                String dbUserName = cursor.getString(cursor.getColumnIndex("name"));
                if (dbUserName.equals(userName)) {
                    Integer dbUserId = cursor.getInt(cursor.getColumnIndex("id"));

                    if(dbUserId != null){
                        userId = dbUserId;
                    }
                    else{
                        userId = -3;
                    }
                }
            }
            cursor.close();
        }

        return userId;
    }

    // Delete note by given note id
    public boolean removeNote(Integer rId){

        SQLiteDatabase db = this.getWritableDatabase();
        boolean success = false;

        String table = "notetable";
        String whereClause = "id=?";
        String[] whereArgs = new String[] { String.valueOf(rId) };
        success = db.delete(table, whereClause, whereArgs) > 0;

        return success;
    }

    // Get all notes that contain a given user id and match a given search string
    public Cursor getNotesByUserId(Integer userId, Integer sortBy, String searchInput) {
        SQLiteDatabase db = this.getReadableDatabase();

        String findStrWhere = " AND name LIKE ?";
        String findStrArg = "%" + searchInput + "%";
        String where;
        String[] whereArgs;

        if((searchInput != null) && (!searchInput.equals(""))) {
            where = "ownerid=?" + findStrWhere;
            whereArgs = new String[]{userId.toString(), findStrArg};
        }
        else{
            where = "ownerid=?";
            whereArgs = new String[]{userId.toString()};
        }

        String from[] = { "id AS _id, name, expdate, date, time, notetype" };
        String sortMethod = "";

        if(sortBy == 0){
            sortMethod = "date DESC, time DESC";
        }
        if(sortBy == 1){
            sortMethod = "case when expdate is null then 1 else 0 end, expdate ASC";
        }
        if(sortBy == 2){
            sortMethod = "name COLLATE NOCASE ASC";
        }
        if(sortBy == 3){
            sortMethod = "date ASC, time ASC";
        }
        if(sortBy == 4){
            sortMethod = "case when expdate is null then 1 else 0 end, expdate DESC";
        }
        if(sortBy == 5){
            sortMethod = "name COLLATE NOCASE DESC";
        }

        Cursor cursor = db.query("notetable", from, where, whereArgs, null, null, sortMethod, null);
        return cursor;
    }

    // Update user note sort preference and return status
    public boolean updateUserSortPref(Integer userId, Integer newSortPref){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_SORTPREF, newSortPref);

        long result = db.update("usertable", contentValues, "id="+userId, null);

        if(result==-1){
            return false;
        }
        else{
            return true;
        }

    }

    // Update user keep me logged in status
    public boolean updateUserStayLoggedPref(Integer userId, Integer stayLoggedPref){

        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(USER_KEEPLOGGED, stayLoggedPref);

        long result = db.update("usertable", contentValues, "id="+userId, null);

        if(result==-1){
            return false;
        }
        else{
            return true;
        }

    }

    // Get keep me logged in status for all users
    public Cursor getAllUsersStayLoggedStatus(){
        SQLiteDatabase db = this.getReadableDatabase();

        String from[] = { "id, name, logged_pref" };

        Cursor cursor = db.query("usertable", from, null, null, null, null, null, null);
        return cursor;
    }
}
