package com.app.mynotes.interfaces;

public interface AdapterButtonListener{
    void containerItemCheck();
    void containerRemoveItem(int item);
}
