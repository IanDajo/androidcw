package com.app.mynotes.interfaces;

// Interface class
public interface OnTaskCompleted{
    void onTaskCompleted(String jokeId, String jokeText);
}
